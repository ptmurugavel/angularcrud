// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  fierbaseConfig: {
    apiKey: 'AIzaSyAA2MUFLPW56iKRaXq5q-qTKXlEtwdgXro',
    authDomain: 'angularfirebasecrud-1b487.firebaseapp.com',
    databaseURL: 'https://angularfirebasecrud-1b487.firebaseio.com',
    projectId: 'angularfirebasecrud-1b487',
    storageBucket: 'angularfirebasecrud-1b487.appspot.com',
    messagingSenderId: '186937855060'
  }

};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
