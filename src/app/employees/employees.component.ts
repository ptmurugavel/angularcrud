import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from './shared/employee.service';
import { AuthService } from './shared/auth/auth.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
  providers: [EmployeeService]
})
export class EmployeesComponent implements OnInit {

  constructor(private employeeService: EmployeeService,
    private authService: AuthService,
    private router: Router) {
  }

  ngOnInit() {
  }

  logOut() {
    this.authService.logout().then(() => {
      this.router.navigate(['/']);
    });
  }
}
