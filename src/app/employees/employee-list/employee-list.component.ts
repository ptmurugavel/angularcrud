import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared/employee.service';
import { Employee } from '../shared/employee.model';
import { ToastrService } from 'ngx-toastr';
import { EmployeeComponent } from '../employee/employee.component';
import { AngularFireList } from 'angularfire2/database';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employeelist: Employee[];
  constructor(private employeeService: EmployeeService, private tostr: ToastrService) { }

  ngOnInit() {
    const x = this.employeeService.getdata();
    x.snapshotChanges().subscribe(item => {
      this.employeelist = [];
      item.forEach(element => {
        const y = element.payload.toJSON();
        y['$key'] = element.key;
        this.employeelist.push(y as Employee);
      });
    });
  }

  onEdit(emp: Employee) {
    this.employeeService.selectedEmployee = Object.assign({}, emp);
  }

  onDelete(key: string) {
    if (confirm('Are you sure to delete this record?') === true) {
      this.employeeService.deleteEmployee(key);
      this.tostr.warning('Record Deleted Successfully!', 'Employee Register');
    }
  }

  getData(empName?: string) {
    let empList: AngularFireList<any>;
    if (empName != null) {
      empList = this.employeeService.getdatawithID(empName);
    } else {
      empList = this.employeeService.getdata();
    }

    empList.snapshotChanges().subscribe(item => {
      this.employeelist = [];
      item.forEach(element => {
        const y = element.payload.toJSON();
        y['$key'] = element.key;
        this.employeelist.push(y as Employee);
      });
    });
  }
}
