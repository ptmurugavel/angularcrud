import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Observable, from } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: Observable<firebase.User>;
  private userDetails: firebase.User;
  public isAuthenticated: boolean;

  constructor(private afAuth: AngularFireAuth) {
    this.user = afAuth.authState;
    this.user.subscribe((user) => {
      if (user) {
        this.userDetails = user;
      } else {
        this.userDetails = null;
      }
    });
  }

  isLoggedIn() {
    return this.afAuth.authState.pipe(
      take(1),
      map(state => !!state),
      tap(authenticated => {
        this.isAuthenticated = authenticated;
        return authenticated;
      })
    );
  }

  verifyAuthenticationStatus() {
    if (this.user == null) {
      return false;
    } else {
      return true;
    }
  }

  loginWithGoogle() {
    return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  login(username, password): Observable<any> {
    return from(
      this.afAuth.auth.signInWithEmailAndPassword(username, password)
    );
  }

  logout() {
    return this.afAuth.auth.signOut();
  }
}
