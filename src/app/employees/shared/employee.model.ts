export class Employee {
    $key: string;
    name: string;
    position: string;
    location: string;
    salary: number;
}
