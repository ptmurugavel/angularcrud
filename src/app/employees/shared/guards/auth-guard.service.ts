import { Injectable, NgZone } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(private router: Router,
    private authService: AuthService,
    private ngZone: NgZone) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let isLoggedIn = false;
    this.authService.isLoggedIn().subscribe(loginStatus => {
      isLoggedIn = loginStatus;
      if (!isLoggedIn) {
        this.ngZone.run(() => {
          this.router.navigate(['/login']);
        });
      }
      return isLoggedIn;
    });
    return this.authService.isLoggedIn();
  }
}
