import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Employee } from './employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  employeelist: AngularFireList<any>;
  selectedEmployee: Employee = new Employee();
  constructor(private firebase: AngularFireDatabase) { }

  getdata() {
    this.employeelist = this.firebase.list('employees');
    return this.employeelist;
  }

  getdatawithID(empName: string) {
    this.employeelist = this.firebase.list('employees', ref => ref.orderByChild('name').equalTo(empName));
    return this.employeelist;
  }

  insertEmployee(employee: Employee) {
    this.employeelist.push({
      name: employee.name,
      position: employee.position,
      location: employee.location,
      salary: employee.salary
    });
  }

  updateEmployee(employee: Employee) {
    this.employeelist.update(employee.$key, {
      name: employee.name,
      position: employee.position,
      location: employee.location,
      salary: employee.salary
    });
  }

  deleteEmployee($key: string) {
    this.employeelist.remove($key);
  }
}
