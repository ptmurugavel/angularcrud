import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeListComponent } from './employees/employee-list/employee-list.component';
import { AuthGuardService } from './employees/shared/guards/auth-guard.service';

export const appRoutes: Routes = [
    {
        path: '', redirectTo: '/login', pathMatch: 'full'
    }, {
        path: 'login',
        component: LoginComponent
    }, {
        path: 'employees',
        canActivate: [AuthGuardService],
        component: EmployeesComponent
    }
];

export const AppRoutes = RouterModule.forRoot(appRoutes);
