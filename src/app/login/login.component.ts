import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../employees/shared/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  public form: FormGroup;
  isLoggedIn: boolean;

  constructor(
    private FB: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private ngZone: NgZone) {
    this.form = this.FB.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  loginWithGoogle() {
    this.authService.loginWithGoogle().then((result) => {
      this.ngZone.run(() => {
        this.router.navigate(['/employees']);
      });
    }).catch((error) => {

    });
  }

  login() {
    const inputvalue = this.form.value;
    this.authService.login(inputvalue.username, inputvalue.password)
      .subscribe(
        success => this.router.navigate(['/employees']),
        error => alert(error)
      );
  }
}

